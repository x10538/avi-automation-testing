package com.everis.avi.objetos;

public class ejecucion {
    int iCodigoEjecucion;
    String sUsuario;
    String sContrasenia;
    String sIndicadorWs;
    String cIndicadorTipoComparacion;
    int iCodigoChatBot;
    int iCodigoDiccionario;
    int iEstadoEjecucion;
    String cAmbiente;

    public String getcAmbiente() {
        return cAmbiente;
    }

    public void setcAmbiente(String cAmbiente) {
        this.cAmbiente = cAmbiente;
    }

    public int getiCodigoEjecucion() {
        return iCodigoEjecucion;
    }

    public void setiCodigoEjecucion(int iCodigoEjecucion) {
        this.iCodigoEjecucion = iCodigoEjecucion;
    }

    public String getsUsuario() {
        return sUsuario;
    }

    public void setsUsuario(String sUsuario) {
        this.sUsuario = sUsuario;
    }

    public String getsContrasenia() {
        return sContrasenia;
    }

    public void setsContrasenia(String sContrasenia) {
        this.sContrasenia = sContrasenia;
    }

    public String getsIndicadorWs() {
        return sIndicadorWs;
    }

    public void setsIndicadorWs(String sIndicadorWs) {
        this.sIndicadorWs = sIndicadorWs;
    }

    public int getiCodigoChatBot() {
        return iCodigoChatBot;
    }

    public void setiCodigoChatBot(int iCodigoChatBot) {
        this.iCodigoChatBot = iCodigoChatBot;
    }

    public int getiCodigoDiccionario() {
        return iCodigoDiccionario;
    }

    public void setiCodigoDiccionario(int iCodigoDiccionario) {
        this.iCodigoDiccionario = iCodigoDiccionario;
    }

    public int getiEstadoEjecucion() {
        return iEstadoEjecucion;
    }

    public void setiEstadoEjecucion(int iEstadoEjecucion) {
        this.iEstadoEjecucion = iEstadoEjecucion;
    }

    public String getcIndicadorTipoComparacion() {
        return cIndicadorTipoComparacion;
    }

    public void setcIndicadorTipoComparacion(String cIndicadorTipoComparacion) {
        this.cIndicadorTipoComparacion = cIndicadorTipoComparacion;
    }
}
