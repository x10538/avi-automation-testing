package com.everis.avi.rest.task;

import com.everis.avi.util.ReadFile;
import com.everis.avi.util.Util;
import com.everis.avi.util.Variables;
import io.restassured.http.ContentType;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

public class DialogoAvi_Beta_ENV implements Task {

    Variables getConfigProperties = new Variables();

    private static String URL = "http://52.179.161.88/conversations/";
    private static final String TEMPLATE_DIALOGO = "/templates/dialogo.json";
    //private static final String ENV = "C://jenkins//enviroment.txt";
    //private static final String NUMBER = "C://jenkins//number.txt";

    private String TextoDilogoAvi;
    private String keyDialogo;
   // private String envFinal = ReadFile.readFile(ENV);
    //private String numFinal = ReadFile.readFile(NUMBER);
   private String envFinal = getConfigProperties.getVariableSerenity("env");
    private String numFinal = getConfigProperties.getVariableSerenity("numero");
    private String userRef = "";
    private String keyHeader = "";

    public DialogoAvi_Beta_ENV(String TextoDilogoAvi, String keyDialogo) {

        if (envFinal.equals("DEV")) {
            if(numFinal.equals("automatico")) {
                userRef = "51999999999";
            }
            else {
                userRef = numFinal;
            }
            keyHeader ="688d57cc3d7e47e2a1e9b5b0393c84db";
        }

        if (envFinal.equals("UAT")) {
            if(numFinal.equals("automatico")) {
                userRef = "51987659101";
            }
            else {
                userRef = numFinal;
            }

            keyHeader ="62b5f305570c41aeb8b90f40e1b1f3e6";

        }
        if (envFinal.equals("UAT2")) {
            if(numFinal.equals("automatico")) {
                userRef = "51987659101";
            }
            else {
                userRef = numFinal;
            }

            keyHeader ="c9664a560c184e8cb857e5d2a7efabc4";
        }
        if (envFinal.equals("STG")) {
            if(numFinal.equals("automatico")) {
                userRef = "51987659101";
            }
            else {
                userRef = numFinal;
            }

            keyHeader ="534cbfdafd7b438ba07cbaf250ff094d";

        }

        this.TextoDilogoAvi = TextoDilogoAvi;
        this.keyDialogo = keyDialogo;
    }

    public static Performable withDialogo(String TextoDilogoAvi, String keyDialogo) {
        return Tasks.instrumented(DialogoAvi_Beta_ENV.class, TextoDilogoAvi, keyDialogo);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to(keyDialogo).with(request -> request
                .contentType(ContentType.JSON)
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .header("API-KEY", "20dcb8cb-4603-4efa-a78a-8bb4f83ce46a")
                .header("PROJECT", "EXT_CONTACT")
                .header("CHANNEL", "WAPP_CC")
                .header("OS", "Windows")
                .header("USER-REF", userRef)
                .header("LOCALE", "es-ES")
                .header("OS-VERSION", "10")
                .header("BROWSER", "Chrome")
                .header("BROWSER-VERSION", "10")
                .header("BUSINESS-KEY", "123")
                .header("Ocp-Apim-Subscription-Key", keyHeader)
                .body(Util.getTemplate(TEMPLATE_DIALOGO)
                        .replace("{text}", TextoDilogoAvi))));
    }

}