package com.everis.avi.rest.task;

import com.everis.avi.util.Util;
import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

public class DialogoAvi_Beta implements Task {

    private static String URL = "http://52.179.161.88/conversations/";
    private static final String TEMPLATE_DIALOGO = "/templates/dialogo.json";

    private String TextoDilogoAvi;
    private String keyDialogo;

    public DialogoAvi_Beta(String TextoDilogoAvi, String keyDialogo) {
        this.TextoDilogoAvi = TextoDilogoAvi;
        this.keyDialogo = keyDialogo;
    }

    public static Performable withDialogo(String TextoDilogoAvi, String keyDialogo) {
        return Tasks.instrumented(DialogoAvi_Beta.class, TextoDilogoAvi, keyDialogo);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to(keyDialogo).with(request -> request
                .contentType(ContentType.JSON)
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .header("API-KEY", "20dcb8cb-4603-4efa-a78a-8bb4f83ce46a")
                .header("PROJECT", "EXT_CONTACT")
                .header("CHANNEL", "WAPP_CC")
                .header("OS", "Windows")
                //DEV
                .header("USER-REF", "51995757703")
                //UAT
                //.header("USER-REF", "51987659101")
                .header("LOCALE", "es-ES")
                .header("OS-VERSION", "10")
                .header("BROWSER", "Chrome")
                .header("BROWSER-VERSION", "10")
                .header("BUSINESS-KEY", "123")
                //KEY DEV
                .header("Ocp-Apim-Subscription-Key", "688d57cc3d7e47e2a1e9b5b0393c84db")
                //KEY UAT
                //.header("Ocp-Apim-Subscription-Key", "62b5f305570c41aeb8b90f40e1b1f3e6")
                //NUEVO KEY UAT
                //.header("Ocp-Apim-Subscription-Key", "c9664a560c184e8cb857e5d2a7efabc4")
                //NUEVO KEY STG
                //.header("Ocp-Apim-Subscription-Key", "534cbfdafd7b438ba07cbaf250ff094d")
                .body(Util.getTemplate(TEMPLATE_DIALOGO)
                        .replace("{text}", TextoDilogoAvi))));
    }
}