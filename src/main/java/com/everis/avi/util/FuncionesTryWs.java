package com.everis.avi.util;

import com.everis.avi.objetos.diccionario;
import com.everis.avi.objetos.ejecucion;
import com.everis.avi.web.page.LoginPage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class FuncionesTryWs {

    int iCodigoChatBot = 0;
    int iCodigoDiccionario = 0;
    int iCodigoEjecucion = 0;
    int iContador = 1;

    String sCodigoBot = "";
    String sCodigoRespuesta = "";
    String sDialogoTry = "";
    String sUsuario = "";
    String sIndidadorWS = "";
    String sIndicadorTipoValidacion = "";
    String sContrasenia = "";
    List<ejecucion> Ejecucion = new ArrayList<>();
    List<diccionario> Diccionario = new ArrayList<>();

    public void ObtenerDatosLogin() throws SQLException, InterruptedException {
        Ejecucion = new Conexion().ObtenerDatosCofiguracion();
        for (ejecucion element : Ejecucion) {
            iCodigoEjecucion = element.getiCodigoEjecucion();
            iCodigoChatBot = element.getiCodigoChatBot();
            iCodigoDiccionario = element.getiCodigoDiccionario();
            sContrasenia = element.getsContrasenia();
            sUsuario = element.getsUsuario();
            sIndidadorWS = element.getsIndicadorWs();
            sIndicadorTipoValidacion = element.getcIndicadorTipoComparacion();

            new Conexion().ResgistrarDatosTemporal(iCodigoChatBot, iCodigoDiccionario);
            Thread.sleep(2000);
            new LoginPage().AccedeLogin(sUsuario, sContrasenia);
            Thread.sleep(15000);
            new LoginPage().QuitarAlertas();
            if (sIndidadorWS.equalsIgnoreCase("SI")) {
                new LoginPage().SeleccionarTarjetaWs();
                Thread.sleep(4000);
                new LoginPage().ConfigurarDIalogoWs();
            } else {
                Thread.sleep(5000);
                new LoginPage().SeleccionarTarjeta();
            }
            new LoginPage().AbrirchatTry();

            Thread.sleep(4000);
            Diccionario = new Conexion().ObtnerDiccionario(iCodigoChatBot, iCodigoDiccionario);
            for (diccionario elementdic : Diccionario) {
                if (elementdic.getvAsesor() != null) {
                    sDialogoTry = elementdic.getvAsesor();
                } else {
                    sDialogoTry = elementdic.getvBoton();
                }
                new LoginPage().EscribirChat(sDialogoTry);
                Thread.sleep(4000);

                sCodigoBot = new Util().EliminarCaracteresExtranios(elementdic.getcCodigoRespuesta());
                sCodigoRespuesta = new Util().EliminarCaracteresExtranios(new LoginPage().ObtenerRespuestaTry(iContador));
                if (sCodigoBot.equalsIgnoreCase(sCodigoRespuesta)) {
                    new Conexion().ActualizarDialogoTemporal(elementdic.getiCodigoDiccionarioHistorico(), "OK");
                    new Conexion().GuardarRespuestaObtenida(elementdic.getiCodigoDiccionarioHistorico(), "", sCodigoRespuesta);
                } else {
                    new Conexion().ActualizarDialogoTemporal(elementdic.getiCodigoDiccionarioHistorico(), "ERROR");
                    new Conexion().GuardarRespuestaObtenida(elementdic.getiCodigoDiccionarioHistorico(), "", sCodigoRespuesta);
                }
                iContador++;
            }
            new Conexion().ResgistrarDatosHistorico(iCodigoEjecucion);
        }
    }
}