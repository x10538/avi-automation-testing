package com.everis.avi.util;

import com.everis.avi.objetos.diccionario;
import com.everis.avi.objetos.ejecucion;
import com.everis.avi.rest.question.CheckAnswer;
import com.everis.avi.rest.task.DialogoAvi;
import com.everis.avi.rest.task.DialogoAvi_Beta;
import com.everis.avi.rest.task.DialogoAvi_Beta_ENV;
import org.junit.Assert;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class FuncionesServicios {
    int ValidadorError = 0;
    String LogError = "";
    int iCodigoChatBot = 0;
    int iCodigoDiccionario = 0;
    int iCodigoEjecucion = 0;
    String iUsuario = "";
    String sIndidadorWS = "";
    String sIndicadorTipoValidacion = "";

    String KeyDiccionario = "";
    String DialigoServicio = "";
    String RespuestaObtenida = "";
    String CodigoRespuestaObtenido = "";
    String RespuestaBot = "";
    String CodigoRespuestaBot = "";
    List<diccionario> Diccionario = new ArrayList<>();
    List<ejecucion> Ejecucion = new ArrayList<>();

    //Validacion por texto de API de WS ENV
    public void ResalizarConversacionWsTextENV(int iCodigoChatBot, int iCodigoDiccionario, int iCodigoEjecucion) throws SQLException {
        Diccionario = new Conexion().ObtnerDiccionario(iCodigoChatBot, iCodigoDiccionario);
        theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta_ENV.withDialogo("HOLA", KeyDiccionario));
        KeyDiccionario = new CheckAnswer().answeredKey();
        theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta_ENV.withDialogo("DNI 77777777", KeyDiccionario));
        for (diccionario element : Diccionario) {
            if (element.getvAsesor() != null) {
                DialigoServicio = element.getvAsesor();
            } else {
                DialigoServicio = element.getvBoton();
            }
            theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta_ENV.withDialogo(DialigoServicio, KeyDiccionario));
            RespuestaObtenida = new Util().EliminarCaracteresExtranios(new CheckAnswer().answeredRespuesta());
            CodigoRespuestaObtenido = new Util().EliminarCaracteresExtranios(new CheckAnswer().answeredCodigo());
            RespuestaBot = new Util().EliminarCaracteresExtranios(element.getvBot());
            if (RespuestaBot.equalsIgnoreCase(RespuestaObtenida)) {
                new Conexion().ActualizarDialogoTemporal(element.getiCodigoDiccionarioHistorico(), "OK");
                new Conexion().GuardarRespuestaObtenida(element.getiCodigoDiccionarioHistorico(), RespuestaObtenida, CodigoRespuestaObtenido);
                ValidadorError = 0;
            } else {
                new Conexion().ActualizarDialogoTemporal(element.getiCodigoDiccionarioHistorico(), "ERROR");
                new Conexion().GuardarRespuestaObtenida(element.getiCodigoDiccionarioHistorico(), RespuestaObtenida, CodigoRespuestaObtenido);
                ValidadorError = 1;
                LogError += iCodigoEjecucion + "Respuesta obtenida " + RespuestaBot + "Respuesta bot: " + RespuestaObtenida + "\\";
            }
        }
        if (ValidadorError == 1) {
            new Conexion().ActualizarEstadoEjecucion(iCodigoEjecucion, "FALLO", "LogError");
        } else {
            new Conexion().ActualizarEstadoEjecucion(iCodigoEjecucion, "CORRECTO", "EL DICCIONARIO DE EJECUTO CORECTAMENTE");
        }
    }

    //Validacion por texto de API de WS
    public void ResalizarConversacionWsText(int iCodigoChatBot, int iCodigoDiccionario, int iCodigoEjecucion) throws SQLException {
        Diccionario = new Conexion().ObtnerDiccionario(iCodigoChatBot, iCodigoDiccionario);
        theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta.withDialogo("HOLA", KeyDiccionario));
        KeyDiccionario = new CheckAnswer().answeredKey();
        theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta.withDialogo("DNI 77777777", KeyDiccionario));
        for (diccionario element : Diccionario) {
            if (element.getvAsesor() != null) {
                DialigoServicio = element.getvAsesor();
            } else {
                DialigoServicio = element.getvBoton();
            }
            theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta.withDialogo(DialigoServicio, KeyDiccionario));
            RespuestaObtenida = new Util().EliminarCaracteresExtranios(new CheckAnswer().answeredRespuesta());
            CodigoRespuestaObtenido = new Util().EliminarCaracteresExtranios(new CheckAnswer().answeredCodigo());
            RespuestaBot = new Util().EliminarCaracteresExtranios(element.getvBot());
            if (RespuestaBot.equalsIgnoreCase(RespuestaObtenida)) {
                new Conexion().ActualizarDialogoTemporal(element.getiCodigoDiccionarioHistorico(), "OK");
                new Conexion().GuardarRespuestaObtenida(element.getiCodigoDiccionarioHistorico(), RespuestaObtenida, CodigoRespuestaObtenido);
                ValidadorError = 0;
            } else {
                new Conexion().ActualizarDialogoTemporal(element.getiCodigoDiccionarioHistorico(), "ERROR");
                new Conexion().GuardarRespuestaObtenida(element.getiCodigoDiccionarioHistorico(), RespuestaObtenida, CodigoRespuestaObtenido);
                ValidadorError = 1;
                    LogError += iCodigoEjecucion + "Respuesta obtenida " + RespuestaBot + "Respuesta bot: " + RespuestaObtenida + "\\";
            }
        }
        if (ValidadorError == 1) {
            new Conexion().ActualizarEstadoEjecucion(iCodigoEjecucion, "FALLO", "LogError");
        } else {
            new Conexion().ActualizarEstadoEjecucion(iCodigoEjecucion, "CORRECTO", "EL DICCIONARIO DE EJECUTO CORECTAMENTE");
        }
    }
    //dni uat
    //Validacion por Codigo de API de WS ENV
    public void ResalizarConversacionWsCodeENV(int iCodigoChatBot, int iCodigoDiccionario, int iCodigoEjecucion) throws SQLException {
        Diccionario = new Conexion().ObtnerDiccionario(iCodigoChatBot, iCodigoDiccionario);
        theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta_ENV.withDialogo("hola", KeyDiccionario));
        KeyDiccionario = new CheckAnswer().answeredKey();
        //theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta_ENV.withDialogo("DNI 70006693", KeyDiccionario));
        for (diccionario element : Diccionario) {
            if (element.getvAsesor() != null) {
                DialigoServicio = element.getvAsesor();
            } else {
                DialigoServicio = element.getvBoton();
            }
            String nuevoDialogo = new Util().EliminarCaracteresExtranios1(DialigoServicio);

            theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta_ENV.withDialogo(nuevoDialogo, KeyDiccionario));
            RespuestaObtenida = new Util().EliminarCaracteresExtranios(new CheckAnswer().answeredRespuesta());
            CodigoRespuestaObtenido = new Util().EliminarCaracteresExtranios(new CheckAnswer().answeredCodigo());
            CodigoRespuestaBot = new Util().EliminarCaracteresExtranios(element.getcCodigoRespuesta());
            if (CodigoRespuestaBot.trim().equalsIgnoreCase(String.valueOf(CodigoRespuestaObtenido.trim()))) {
                new Conexion().ActualizarDialogoTemporal(element.getiCodigoDiccionarioHistorico(), "OK");
                new Conexion().GuardarRespuestaObtenida(element.getiCodigoDiccionarioHistorico(), RespuestaObtenida, CodigoRespuestaObtenido);
                ValidadorError = 0;
            } else {
                new Conexion().ActualizarDialogoTemporal(element.getiCodigoDiccionarioHistorico(), "ERROR");
                new Conexion().GuardarRespuestaObtenida(element.getiCodigoDiccionarioHistorico(), RespuestaObtenida, CodigoRespuestaObtenido);
                ValidadorError = 1;
                LogError += iCodigoEjecucion + "Respuesta obtenida " + CodigoRespuestaObtenido + "Respuesta bot: " + CodigoRespuestaBot + "\\";
            }
        }

        if (ValidadorError == 1) {
            new Conexion().ActualizarEstadoEjecucion(iCodigoEjecucion, "FALLO", "LogError");
        } else {
            new Conexion().ActualizarEstadoEjecucion(iCodigoEjecucion, "CORRECTO", "EL DICCIONARIO SE EJECUTO CORRECTAMENTE");
        }
    }

    //Validacion por Codigo de API de WS
    public void ResalizarConversacionWsCode(int iCodigoChatBot, int iCodigoDiccionario, int iCodigoEjecucion) throws SQLException {
        Diccionario = new Conexion().ObtnerDiccionario(iCodigoChatBot, iCodigoDiccionario);
        theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta.withDialogo("hola", KeyDiccionario));
        KeyDiccionario = new CheckAnswer().answeredKey();
        //theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta.withDialogo("DNI 70006693", KeyDiccionario));
        for (diccionario element : Diccionario) {
            if (element.getvAsesor() != null) {
                DialigoServicio = element.getvAsesor();
            } else {    
                DialigoServicio = element.getvBoton();
            }
            String nuevoDialogo = new Util().EliminarCaracteresExtranios1(DialigoServicio);

            theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta.withDialogo(nuevoDialogo, KeyDiccionario));
            RespuestaObtenida = new Util().EliminarCaracteresExtranios(new CheckAnswer().answeredRespuesta());
            CodigoRespuestaObtenido = new Util().EliminarCaracteresExtranios(new CheckAnswer().answeredCodigo());
            CodigoRespuestaBot = new Util().EliminarCaracteresExtranios(element.getcCodigoRespuesta());
            if (CodigoRespuestaBot.trim().equalsIgnoreCase(String.valueOf(CodigoRespuestaObtenido.trim()))) {
                new Conexion().ActualizarDialogoTemporal(element.getiCodigoDiccionarioHistorico(), "OK");
                new Conexion().GuardarRespuestaObtenida(element.getiCodigoDiccionarioHistorico(), RespuestaObtenida, CodigoRespuestaObtenido);
                ValidadorError = 0;
            } else {
                new Conexion().ActualizarDialogoTemporal(element.getiCodigoDiccionarioHistorico(), "ERROR");
                new Conexion().GuardarRespuestaObtenida(element.getiCodigoDiccionarioHistorico(), RespuestaObtenida, CodigoRespuestaObtenido);
                ValidadorError = 1;
                LogError += iCodigoEjecucion + "Respuesta obtenida " + CodigoRespuestaObtenido + "Respuesta bot: " + CodigoRespuestaBot + "\\";


           //     Assert.assertEquals(CodigoRespuestaBot.trim(),CodigoRespuestaObtenido.trim());
            }
        }

        if (ValidadorError == 1) {
            new Conexion().ActualizarEstadoEjecucion(iCodigoEjecucion, "FALLO", "LogError");
        } else {
            new Conexion().ActualizarEstadoEjecucion(iCodigoEjecucion, "CORRECTO", "EL DICCIONARIO SE EJECUTO CORRECTAMENTE");
        }
    }
    //dni dev
    public void ResalizarConversacionCode(int iCodigoChatBot, int iCodigoDiccionario, int iCodigoEjecucion) throws SQLException {
        Diccionario = new Conexion().ObtnerDiccionario(iCodigoChatBot, iCodigoDiccionario);
        theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta.withDialogo("HOLA", KeyDiccionario));
        KeyDiccionario = new CheckAnswer().answeredKey();
        //theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta.withDialogo("ACEPTO", KeyDiccionario));
        theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta.withDialogo("DNI 70006693", KeyDiccionario));


        for (diccionario element : Diccionario) {
            if (element.getvAsesor() != null) {
                DialigoServicio = element.getvAsesor();
            } else {
                DialigoServicio = element.getvBoton();
            }
            theActorInTheSpotlight().attemptsTo(DialogoAvi.withDialogo(DialigoServicio, KeyDiccionario));
            RespuestaObtenida = new Util().EliminarCaracteresExtranios(new CheckAnswer().answeredRespuesta());
            CodigoRespuestaObtenido = new Util().EliminarCaracteresExtranios(new CheckAnswer().answeredCodigo());
            CodigoRespuestaBot = new Util().EliminarCaracteresExtranios(element.getcCodigoRespuesta());
            if (CodigoRespuestaBot.trim().equalsIgnoreCase(String.valueOf(CodigoRespuestaObtenido))) {
                new Conexion().ActualizarDialogoTemporal(element.getiCodigoDiccionarioHistorico(), "OK");
                new Conexion().GuardarRespuestaObtenida(element.getiCodigoDiccionarioHistorico(), RespuestaObtenida, CodigoRespuestaObtenido);
                ValidadorError = 0;
            } else {
                new Conexion().ActualizarDialogoTemporal(element.getiCodigoDiccionarioHistorico(), "ERROR");
                new Conexion().GuardarRespuestaObtenida(element.getiCodigoDiccionarioHistorico(), RespuestaObtenida, CodigoRespuestaObtenido);
                ValidadorError = 1;
                LogError += iCodigoEjecucion + "Respuesta obtenida " + CodigoRespuestaBot + "Respuesta bot: " + CodigoRespuestaBot + "\\";
            }
        }
    }

    public void ResalizarConversacionText(int iCodigoChatBot, int iCodigoDiccionario, int iCodigoEjecucion) throws SQLException {
        Diccionario = new Conexion().ObtnerDiccionario(iCodigoChatBot, iCodigoDiccionario);
        theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta.withDialogo("HOLA", KeyDiccionario));
        KeyDiccionario = new CheckAnswer().answeredKey();
        theActorInTheSpotlight().attemptsTo(DialogoAvi_Beta.withDialogo("DNI 66666666", KeyDiccionario));
        for (diccionario element : Diccionario) {
            if (element.getvAsesor() != null) {
                DialigoServicio = element.getvAsesor();
            } else {
                DialigoServicio = element.getvBoton();
            }
            theActorInTheSpotlight().attemptsTo(DialogoAvi.withDialogo(DialigoServicio, KeyDiccionario));
            RespuestaObtenida = new Util().EliminarCaracteresExtranios(new CheckAnswer().answeredRespuesta());
            CodigoRespuestaObtenido = new Util().EliminarCaracteresExtranios(new CheckAnswer().answeredCodigo());
            RespuestaBot = new Util().EliminarCaracteresExtranios(element.getcCodigoRespuesta());
            if (RespuestaBot.equalsIgnoreCase(RespuestaObtenida)) {
                new Conexion().ActualizarDialogoTemporal(element.getiCodigoDiccionarioHistorico(), "OK");
                new Conexion().GuardarRespuestaObtenida(element.getiCodigoDiccionarioHistorico(), RespuestaObtenida, CodigoRespuestaObtenido);
                ValidadorError = 0;
            } else {
                new Conexion().ActualizarDialogoTemporal(element.getiCodigoDiccionarioHistorico(), "ERROR");
                new Conexion().GuardarRespuestaObtenida(element.getiCodigoDiccionarioHistorico(), RespuestaObtenida, CodigoRespuestaObtenido);
                ValidadorError = 1;
                LogError += iCodigoEjecucion + "Respuesta obtenida " + RespuestaObtenida + "Respuesta bot: " + RespuestaBot + "\\";
            }
        }
        if (ValidadorError == 1) {
            new Conexion().ActualizarEstadoEjecucion(iCodigoEjecucion, "FALLO", "LogError");
        } else {
            new Conexion().ActualizarEstadoEjecucion(iCodigoEjecucion, "CORRECTO", "EL DICCIONARIO SE EJECUTO CORECTAMENTE");
        }
    }

    public void RealizarDialogoSerevicios() throws SQLException {

        Ejecucion = new Conexion().ObtenerDatosCofiguracion();

        for (ejecucion element : Ejecucion) {

            iCodigoEjecucion = element.getiCodigoEjecucion();
            iCodigoChatBot = element.getiCodigoChatBot();
            iCodigoDiccionario = element.getiCodigoDiccionario();
            iUsuario = element.getsUsuario();
            sIndidadorWS = element.getsIndicadorWs();
            sIndicadorTipoValidacion = element.getcIndicadorTipoComparacion();

            new Conexion().ResgistrarDatosTemporal(iCodigoChatBot, iCodigoDiccionario);

            if (sIndidadorWS.toString().trim().equalsIgnoreCase("SI") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("CODE")) {
                new FuncionesServicios().ResalizarConversacionWsCode(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
            } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("SI") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("TEXT")) {
                new FuncionesServicios().ResalizarConversacionWsText(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
            } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("NO") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("CODE")) {
                new FuncionesServicios().ResalizarConversacionCode(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
            } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("NO") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("TEXT")) {
                new FuncionesServicios().ResalizarConversacionText(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
            }
            new Conexion().ResgistrarDatosHistorico(iCodigoEjecucion);
            new Conexion().EliminarDatosTemporal();
            new Conexion().ReiniciarCampoIdentity();
        }
    }
}
