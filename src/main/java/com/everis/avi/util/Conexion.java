package com.everis.avi.util;

import com.everis.avi.objetos.diccionario;
import com.everis.avi.objetos.ejecucion;

import java.sql.*;
import java.util.ArrayList;

public class Conexion {
    private static final String DB = "C://jenkins//database.txt";
    Variables getConfigProperties = new Variables();
    private String dbFinal = getConfigProperties.getVariableSerenity("database");
    public Connection CadenaConexion() {
        Connection cn = null;


        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String connectionUrl ="";
            if(dbFinal.equals("dbautomationT")) {
                connectionUrl = "jdbc:sqlserver://dbautomation2.ddns.net;databaseName=db_automationT;user=sa;password=231400aP01;";
            }
            if(dbFinal.equals("dbautomation0")) {
                 connectionUrl = "jdbc:sqlserver://dbautomation2.ddns.net;databaseName=db_automation0;user=sa;password=231400aP01;";
            }
            if(dbFinal.equals("dbautomation1")) {
                connectionUrl = "jdbc:sqlserver://dbautomation2.ddns.net;databaseName=db_automation1;user=sa;password=231400aP01;";
            }
            if(dbFinal.equals("dbautomation2")) {
                connectionUrl = "jdbc:sqlserver://dbautomation2.ddns.net;databaseName=db_automation2;user=sa;password=231400aP01;";
            }
            if(dbFinal.equals("dbautomation3")) {
                connectionUrl = "jdbc:sqlserver://dbautomation2.ddns.net;databaseName=db_automation3;user=sa;password=231400aP01;";
            }
            if(dbFinal.equals("dbautomation4")) {
                connectionUrl = "jdbc:sqlserver://dbautomation2.ddns.net;databaseName=db_automation4;user=sa;password=231400aP01;";
            }
            if(dbFinal.equals("dbautomation5")) {
                connectionUrl = "jdbc:sqlserver://dbautomation2.ddns.net;databaseName=db_automation5;user=sa;password=231400aP01;";
            }
            cn = DriverManager.getConnection(connectionUrl);

        } catch (SQLException e) {
            System.out.println("SQL Exception: " + e.toString());
        } catch (ClassNotFoundException cE) {
            System.out.println("Class Not Found Exception: " + cE.toString());
        }
        return cn;
    }
    public ArrayList<ejecucion> ObtenerDatosCofiguracionDEV() throws SQLException {
        ArrayList<ejecucion> ejecucionList = new ArrayList<>();
        String SQL = "SELECT * FROM tbl_Ejecucion WHERE iEstadoEjecucion=1";
        Statement stmt = CadenaConexion().createStatement();
        ResultSet rs = stmt.executeQuery(SQL);
        while (rs.next()) {
            ejecucion ejecucion = new ejecucion();
            ejecucion.setiCodigoEjecucion(rs.getInt("iCodigoEjecucion"));
            ejecucion.setsUsuario(rs.getString("vUsuario"));
            ejecucion.setsContrasenia(rs.getString("vContrasenia"));
            ejecucion.setsIndicadorWs(rs.getString("vIndicadorWs"));
            ejecucion.setiCodigoChatBot(rs.getInt("iCodigoBot"));
            ejecucion.setiCodigoDiccionario(rs.getInt("iCodigoDiccionarioTipo"));
            ejecucion.setiEstadoEjecucion(rs.getInt("iEstadoEjecucion"));
            ejecucion.setcIndicadorTipoComparacion(rs.getString("cIndicadorTipoComparacion"));
            ejecucion.setcAmbiente(rs.getString("cAmbiente"));
            ejecucionList.add(ejecucion);
        }
        return ejecucionList;
    }
    public ArrayList<ejecucion> ObtenerDatosCofiguracion() throws SQLException {
        ArrayList<ejecucion> ejecucionList = new ArrayList<>();
        String SQL = "SELECT * FROM tbl_Ejecucion WHERE iEstadoEjecucion=1";
        Statement stmt = CadenaConexion().createStatement();
        ResultSet rs = stmt.executeQuery(SQL);
        while (rs.next()) {
            ejecucion ejecucion = new ejecucion();
            ejecucion.setiCodigoEjecucion(rs.getInt("iCodigoEjecucion"));
            ejecucion.setsUsuario(rs.getString("vUsuario"));
            ejecucion.setsContrasenia(rs.getString("vContrasenia"));
            ejecucion.setsIndicadorWs(rs.getString("vIndicadorWs"));
            ejecucion.setiCodigoChatBot(rs.getInt("iCodigoBot"));
            ejecucion.setiCodigoDiccionario(rs.getInt("iCodigoDiccionarioTipo"));
            ejecucion.setiEstadoEjecucion(rs.getInt("iEstadoEjecucion"));
            ejecucion.setcIndicadorTipoComparacion(rs.getString("cIndicadorTipoComparacion"));
            ejecucion.setcAmbiente(rs.getString("cAmbiente"));
            ejecucionList.add(ejecucion);
        }
        return ejecucionList;
    }

    public ArrayList<diccionario> ObtnerDiccionario(int iCodigoBot, int iCodidoDiccionarioTipo) throws SQLException {
        ArrayList<diccionario> diccionarioList = new ArrayList<>();
        String SQL = "select * from tbl_DiccionarioTemp where iCodigoBot = " + iCodigoBot + " and iCodigoDiccionarioTipo =" + iCodidoDiccionarioTipo + "order by iCodigoDiccionarioTemp asc";

        //String SQL = "select * from tbl_DiccionarioTemp";
        Statement stmt = CadenaConexion().createStatement();
        ResultSet rs = stmt.executeQuery(SQL);
        while (rs.next()) {
            diccionario diccionario = new diccionario();
            diccionario.setiCodigoDiccionarioHistorico(rs.getInt("iCodigoDiccionarioTemp"));
            diccionario.setcPosicionFlujo(rs.getString("cPosicionFlujo"));
            diccionario.setcIndicadorFormulario(rs.getString("cIndicadorFormulario"));
            diccionario.setvAsesor(rs.getString("vAsesor"));
            diccionario.setvBoton(rs.getString("vBoton"));
            diccionario.setvBot(rs.getString("vBot"));
            diccionario.setcCodigoRespuesta(rs.getString("cCodigoRespuesta"));
            diccionario.setvRespuestaObtenida(rs.getString("vRespuestaObtenida"));
            diccionario.setcCodigoObtenido(rs.getString("cCodigoObtenido"));
            diccionario.setcResultado(rs.getString("cResultado"));
            diccionarioList.add(diccionario);
        }
        return diccionarioList;
    }

    public void ActualizarDialogoTemporal(int iCodigoDialogoTemporal, String Respuesta) throws SQLException {
        try {
            PreparedStatement ps = CadenaConexion().prepareStatement(
                    "UPDATE tbl_DiccionarioTemp set cResultado = ? WHERE iCodigoDiccionarioTemp= ?");

            ps.setString(1, Respuesta);
            ps.setInt(2, iCodigoDialogoTemporal);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException se) {
            throw se;
        }

    }

    public void ActualizarEstadoEjecucion(int iCodigoEjecucion, String sEstadoEjecucion, String sDescripcionEjecucion) throws SQLException {
        try {
            PreparedStatement ps = CadenaConexion().prepareStatement(
                    "update tbl_Ejecucion set sEstadoEjecucion = ? , sDescripcionEjecucion = ? where iCodigoEjecucion = ?");

            ps.setString(1, sEstadoEjecucion);
            ps.setString(2, String.valueOf(sDescripcionEjecucion));
            ps.setInt(3, iCodigoEjecucion);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException se) {
            throw se;
        }
    }

    public void GuardarRespuestaObtenida(int iCodigoDialogoTemporal, String Respuesta, String sCodigoObtenido) throws SQLException {
        try {
            PreparedStatement ps = CadenaConexion().prepareStatement(
                    "UPDATE tbl_DiccionarioTemp set vRespuestaObtenida = ?, cCodigoObtenido = ? WHERE iCodigoDiccionarioTemp= ?");

            ps.setString(1, Respuesta);
            ps.setString(2, String.valueOf(sCodigoObtenido));
            ps.setInt(3, iCodigoDialogoTemporal);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException se) {
            throw se;
        }
    }

    public void ResgistrarDatosHistorico(int iCodigoEjecucion) {
        try {
            Statement st = CadenaConexion().createStatement();
            st.executeUpdate("INSERT INTO tbl_DiccionarioHistorico(cPosicionFlujo,cIndicadorFormulario,vAsesor,vBot,cCodigoRespuesta,vRespuestaObtenida,cCodigoObtenido,cResultado,iCodigoDiccionarioTipo,iCodigoBot,iCodigoEjecucion) " +
                    "SELECT cPosicionFlujo,cIndicadorFormulario,vAsesor,vBot,cCodigoRespuesta,vRespuestaObtenida,cCodigoObtenido,cResultado,iCodigoDiccionarioTipo,iCodigoBot," + iCodigoEjecucion + " FROM tbl_DiccionarioTemp order by iCodigoDiccionarioTemp asc");
            CadenaConexion().close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }


    public void CrearDatosNuevoDiccionario() throws SQLException {
        String varSessionId = "";
        String varSessionIdNext = "";
        String cCodigoRespuesta = "";
        String cCodigoConcat = "";
        String vPreguntaResp = "";

        String cCodigoRespuestaTemp = "";
        String SQL = "SELECT * FROM tbl_DiccionarioConv";

        Statement stmt = CadenaConexion().createStatement();
        ResultSet rs = stmt.executeQuery(SQL);

        while (rs.next()) {
            varSessionId = rs.getString("vSessionId");

            cCodigoRespuestaTemp = rs.getString("cCodigoRespuesta");
            cCodigoRespuestaTemp = cCodigoRespuestaTemp.replaceAll("\\s+", "");

            if (cCodigoRespuestaTemp.equals("Pregunta"))
                vPreguntaResp = rs.getString("vPreguntaRespuesta");


            //Comparo si el codigo identificador es igual
            if (varSessionId.equals(varSessionIdNext)) {//Si es igual sigue el flujo sin insertar
            } else {
                //inserto fila
                try {
                    Statement st = CadenaConexion().createStatement();
                    st.executeUpdate("INSERT INTO tbl_DiccionarioConvertido(cPosicionFlujo,cIndicadorFormulario,vAsesor,vBot,cCodigoRespuesta,vRespuestaObtenida,cCodigoObtenido,cResultado,iCodigoDiccionarioTipo,iCodigoBot,iCodigoUsuario,iCodigoEjecucion) " +
                            "VALUES ('CP00', '', '" + vPreguntaResp + "','','" + cCodigoConcat + "','', '','', 15,5, '', 15);");
                    CadenaConexion().close();
                } catch (Exception e) {
                    System.err.println("Got an exception!!");
                    System.err.println(e.getMessage());
                }
                //Limpiar valores
                cCodigoConcat = "";
            }
            if (cCodigoRespuesta.equals("Pregunta")) {
                //No concatenar el codigo
                //inserto fila
                try {
                    Statement st = CadenaConexion().createStatement();
                    st.executeUpdate("INSERT INTO tbl_DiccionarioConvertido(cPosicionFlujo,cIndicadorFormulario,vAsesor,vBot,cCodigoRespuesta,vRespuestaObtenida,cCodigoObtenido,cResultado,iCodigoDiccionarioTipo,iCodigoBot,iCodigoUsuario,iCodigoEjecucion) " +
                            "VALUES ('CP00', '', ' " + vPreguntaResp + " ','', '" + cCodigoConcat + "' ,'', '','', 15,5, '', 15);");
                    CadenaConexion().close();
                } catch (Exception e) {
                    System.err.println("Got an exception! ");
                    System.err.println(e.getMessage());
                }
                //Limpiar valores
                cCodigoConcat = "";
            } else {
                //Concateno

                cCodigoConcat = cCodigoConcat + cCodigoRespuesta;
                System.out.println(cCodigoConcat);
            }
            //Obtengo la nueva id
            varSessionIdNext = rs.getString("vSessionId");
            //asigno a la variable pregunta
            cCodigoRespuesta = rs.getString("cCodigoRespuesta");
            cCodigoRespuesta = cCodigoRespuesta.replaceAll("\\s+", "");

        }
    }

    public void ResgistrarDatosTemporal(int iCodigoBot, int iCodidoDiccionarioTipo) {
        try {

            Statement st = CadenaConexion().createStatement();
            st.executeUpdate("INSERT INTO tbl_DiccionarioTemp(cPosicionFlujo,cIndicadorFormulario,vAsesor,vBot,cCodigoRespuesta,vRespuestaObtenida,cCodigoObtenido,cResultado,iCodigoDiccionarioTipo,iCodigoBot)\n" +
                    "SELECT cPosicionFlujo,cIndicadorFormulario,vAsesor,vBot,cCodigoRespuesta,vRespuestaObtenida,cCodigoObtenido,cResultado,iCodigoDiccionarioTipo,iCodigoBot FROM tbl_DiccionarioPlantilla\n" +
                    "WHERE iCodigoBot = " + iCodigoBot + " AND iCodigoDiccionarioTipo =" + iCodidoDiccionarioTipo + "order by iCodigoDiccionarioPlantilla asc");
            CadenaConexion().close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }

    public void EliminarDatosTemporal() {
        Statement stmt = null;
        try {

            stmt = CadenaConexion().createStatement();
            stmt.execute("DELETE FROM tbl_DiccionarioTemp");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                CadenaConexion().close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void ReiniciarCampoIdentity() {
        Statement stmt = null;
        try {
            stmt = CadenaConexion().createStatement();
            stmt.execute("DBCC CHECKIDENT (tbl_DiccionarioTemp, RESEED, 1)");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                CadenaConexion().close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
