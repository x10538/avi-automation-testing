package com.everis.avi.web.steps;

import com.everis.avi.objetos.ejecucion;
import com.everis.avi.util.FuncionesWhatsappWeb;
import com.everis.avi.web.fact.AccessAviTry;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actions.Open;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class DialogoWhatsappWebToNumber {
    int iCanidadEjecucion = 0;
    List<ejecucion> Ejecucion = new ArrayList<>();

    @Given("^The (.*) actor obtains the configuration data for the execution of the dictionaries and enter to Whatsapp Web direct message number$")
    public void theSantiagoActorObtainsTheConfigurationDataForTheExecutionOfTheDictionaries(String Actorname) throws Throwable {
       theActorCalled(Actorname).has(AccessAviTry.toAviTry());

        theActorInTheSpotlight().attemptsTo(Open.url("https://wa.me/51950276376"));
        Thread.sleep(3000);

        new FuncionesWhatsappWeb().IngresarChatNumero();
    }
    @When("^Enter to number chat and try and perform the conversion to get the response codes$")
    public void enterWatsonTryAndPerformTheConversionToGetTheResponseCodes() throws SQLException, InterruptedException {
        Thread.sleep(15000);

        new FuncionesWhatsappWeb().RealizarConversacion();
    }

}